import logging
import re

from urllib.parse import quote

import requests

from mycroft import MycroftSkill, intent_handler
from mycroft.messagebus import Message
from mycroft.util.log import LOG

logger = logging.getLogger(__name__)


class YouTubeNotFoundError(Exception):
    pass


class YouTube:

    SEARCH_URL = (
        "https://www.googleapis.com/youtube/v3/search"
        "?part=snippet"
        "&maxResults=1"
        "&q={query}"
        "&safeSearch=none"
        "&order=relevance"
        "&type=video"
        "&fields=items%2Fid%2FvideoId%2Citems%2Fsnippet%2Ftitle"
        "&key={key}"
    )
    WATCH_REGEX = re.compile(r"^/watch\?")

    def __init__(self, api_key: str):
        self.api_key = api_key
        self.logger = LOG.create_logger(self.__class__.__name__)

    def search(self, query: str) -> str:
        """
        Details here:
          https://developers.google.com/youtube/v3/code_samples/code_snippets
        """

        if not self.api_key:
            raise YouTubeNotFoundError(
                "Youtube search is unavailable due to the absence of a "
                "Youtube key.  Visit home dot mycroft dot ai to configure "
                "this skill with your key."
            )

        self.logger.info("Searching YouTube for %s", query)

        try:
            url = self.SEARCH_URL.format(query=quote(query), key=self.api_key)
            response = requests.get(url).json()
            for video in response["items"]:
                self.logger.info(video["snippet"]["title"])
            video_id = response["items"][0]["id"]["videoId"]
        except (KeyError, IndexError):
            raise YouTubeNotFoundError(
                "YouTube responded with something we couldn't understand."
            )

        return f"https://www.youtube.com/watch?v={video_id}"


class YoutubeSkill(MycroftSkill):
    """
    Messages emitted:
      * skill.majel.browser.open
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        key = self.settings.get("api_key")
        if not key:
            self.log.warning(
                "Youtube hasn't been configured with an API key.  You need to "
                "get one from Google (they're free) and then enter the key by "
                "going to https://home.mycroft.ai/ and typing it into the "
                'Youtube skill under "skill settings".'
            )
        self.youtube = YouTube(api_key=key)

    @intent_handler("youtube.intent")
    def handle_youtube(self, message):

        query = message.data.get("query")
        if not query:
            self.speak("Sorry, I couldn't understand")
            return

        try:
            self.log.info("Sending to skill.majel.browser.open")
            self.bus.emit(
                Message(
                    "skill.majel.browser.open",
                    {"url": self.youtube.search(query)},
                )
            )
        except YouTubeNotFoundError as e:
            self.speak(str(e).replace("_", " ").replace("API", "ay pee eye"))
            return

        self.speak_dialog("youtube")


def create_skill():
    return YoutubeSkill()
