# youtube-skill

A Mycroft skill that leverages [Majel](https://pypi.org/project/majel/) to find
& play Youtube videos for you.


## Configuration

Talking to Youtube means using Google's API services, and to do that, you need
a (free) API key from them.  Simply visit this [Google help page](https://developers.google.com/youtube/v3/getting-started)
for instructions on how to use their developer's console and generate a key.
Once you have a key, go to [home.mycroft.ai](https://home.mycroft.ai/) and edit
the settings for this skill.  Put your newly-created key into the box labelled
"Youtube API Key".


## Use

Try one of the following:

> *"Hey Mycroft, youtube baby shark"* - This should open Youtube and play that
> video my kid won't stop screaming about... on a loop.  You're welcome.

> *"Hey Mycroft, youtube the passion of the nerd why you should watch buffy"* -
> This is a great video.  Ian is a fantastic writer.
